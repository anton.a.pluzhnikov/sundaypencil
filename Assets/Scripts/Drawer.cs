﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Drawer : MonoBehaviour
{
    [SerializeField] private RawImage rawImage;

    private Texture2D texture;
    private Color selectedColor = Color.black;
    private int thickness = 3;
    private Vector2 lastMousePosition;
    
    private void Start()
    {
        texture = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
        texture.SetPixels(0, 0, Screen.width, Screen.height, GetColorArray(Screen.width * Screen.height, Color.white));
        texture.Apply();
        rawImage.texture = texture;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            lastMousePosition = Input.mousePosition;
        }
        
        if (Input.GetMouseButton(0))
        {
            DrawLine(lastMousePosition, Input.mousePosition, thickness, selectedColor);
            texture.Apply();
            lastMousePosition = Input.mousePosition;
        }
    }

    private void DrawLine(Vector2 start, Vector2 end, int thickness, Color color)
    {
        var currentPoint = start;
        var diagonalUnit = 1 / Mathf.Max(Mathf.Abs(start.x - end.x), Mathf.Abs(start.y - end.y));
        var lerp = 0.0f;
        
        while (lerp < 1)
        {
            currentPoint = Vector2.Lerp(start, end, lerp);
            lerp += diagonalUnit;
            DrawPoint(currentPoint, thickness, color);
        }
    }
    
    private void DrawPoint(Vector2 position, int thickness, Color color)
    {
        var colors = GetColorArray(thickness * thickness, color);

        if ((int) position.x > 0 && 
            (int) position.y > 0 && 
            (int) position.x < texture.width - 1 && 
            (int) position.y < texture.height - 1)
        {
            texture.SetPixels((int)position.x - 1,(int)position.y - 1,thickness,thickness,colors);
        }
    }

    private Color[] GetColorArray(int length, Color color)
    {
        var colors = new Color[length];
        for (var index = 0; index < colors.Length; index++)
        {
            colors[index] = color;
        }

        return colors;
    }
}
